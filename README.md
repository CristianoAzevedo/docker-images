PHP 7.4, 7.3, 7.2 with composer, Xdebug, imagick and with the following modules:

[PHP Modules]
**amqp**
**bcmath**
**Core**
**ctype**
**curl**
**date**
**dom**
**fileinfo**
**filter**
**ftp**
**hash**
**iconv**
**imagick**
**intl**
**json**
**libxml**
**mbstring**
**mysqlnd**
**openssl**
**pcre**
**PDO**
**pdo_mysql**
**pdo_pgsql**
**pdo_sqlite**
**Phar**
**posix**
**readline**
**redis**
**Reflection**
**session**
**SimpleXML**
**sockets**
**sodium**
**SPL**
**sqlite3**
**standard**
**tokenizer**
**xdebug**
**xml**
**xmlreader**
**xmlwriter**
**zip**
**zlib**

[Zend Modules]
**Xdebug**